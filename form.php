<?php
require('includes/header.php');
?>
<div class="container">
  <h1>Register <small>Enter your details</small></h1>
  <hr/>
  <form>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="firstname">First Name</label>
          <input type="text" class="form-control" id="firstname" placeholder="First Name">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="lastname">Last Name</label>
          <input type="text" class="form-control" id="lastname" placeholder="Last Name">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" placeholder="Email address">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="checkbox">
          <label>
            <input type="checkbox"> I Accept the terms and conditions
          </label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <button type="submit" class="btn btn-default">Signup</button>
      </div>
    </div>

    </div>
  </form>
</div>
<?php
require('includes/footer.php');
?>