<?php
require('includes/header.php');
?>
<div class="container">
  <h1>Gallery <small>Image gallery</small></h1>
  <hr/>
  <div class="row">
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img src="http://via.placeholder.com/400x300" alt="dummy image">
      </a>
    </div>
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img src="http://via.placeholder.com/400x300" alt="dummy image">
      </a>
    </div>
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img src="http://via.placeholder.com/400x300" alt="dummy image">
      </a>
    </div>
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img src="http://via.placeholder.com/400x300" alt="dummy image">
      </a>
    </div>
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img src="http://via.placeholder.com/400x300" alt="dummy image">
      </a>
    </div>
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img src="http://via.placeholder.com/400x300" alt="dummy image">
      </a>
    </div>
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img src="http://via.placeholder.com/400x300" alt="dummy image">
      </a>
    </div>
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img src="http://via.placeholder.com/400x300" alt="dummy image">
      </a>
    </div>
  </div>
</div>
<?php
require('includes/footer.php');
?>